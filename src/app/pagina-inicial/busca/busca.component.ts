import { BuscaPontosVendaService } from './../../shared/services/busca-pontos-venda.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { PontoVendaModel } from '../../shared/models/ponto-venda.model';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-busca',
  templateUrl: './busca.component.html',
  styleUrls: ['./busca.component.css']
})
export class BuscaComponent implements OnInit {

  pontosVendaObservable: Observable<PontoVendaModel[]>;
  nome = new FormControl();


  constructor(private buscaPontosVendaService: BuscaPontosVendaService) { }

  ngOnInit() {
  }

  buscarPontoVendaPorNome(nome: String) {
    console.log(nome);
    this.pontosVendaObservable = this.buscaPontosVendaService.buscarPontoVendaPorNome(nome);
  }

}
