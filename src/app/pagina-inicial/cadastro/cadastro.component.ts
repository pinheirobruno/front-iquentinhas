import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { AccountService } from '../../shared/services/account.service';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit {

  formCadastroUsuario: FormGroup;

  constructor(private formBuilder: FormBuilder, private accountService: AccountService) { }

  ngOnInit() {
    this.formCadastroUsuario = this.formBuilder.group({
      nomeCompleto: [null],
      email: [null],
      telefone: [null],
      senha: [null]
    });
  }

  onSubmit(){    
    this.accountService.cadastrarUsuario(this.formCadastroUsuario);
  }

  cadastrarUsuario(nome: string, email: string, telefone: string, senha: string){

  }

}
