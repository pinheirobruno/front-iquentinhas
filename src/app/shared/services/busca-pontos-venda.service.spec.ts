import { TestBed, inject } from '@angular/core/testing';

import { BuscaPontosVendaService } from './busca-pontos-venda.service';

describe('BuscaPontosVendaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BuscaPontosVendaService]
    });
  });

  it('should be created', inject([BuscaPontosVendaService], (service: BuscaPontosVendaService) => {
    expect(service).toBeTruthy();
  }));
});
