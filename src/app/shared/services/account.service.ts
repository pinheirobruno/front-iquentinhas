import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UsuarioModel } from '../models/usuario.model';
import { FormGroup } from '@angular/forms/src/model';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class AccountService {

  constructor(private httpClient: HttpClient) { }

  cadastrarUsuario(formGroupCadastrarUsuario: FormGroup) {
    console.log(JSON.stringify(formGroupCadastrarUsuario.value));
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    this.httpClient.post<UsuarioModel>('http://localhost:49478/api/Usuario/CriarUsuario',
      JSON.stringify(formGroupCadastrarUsuario.value),
      {headers: headers})
      .map(res => JSON.stringify(res))
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'))
      .subscribe();
  }

}
