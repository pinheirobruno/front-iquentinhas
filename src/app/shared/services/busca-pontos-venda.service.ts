import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { PontoVendaModel } from '../models/ponto-venda.model';
import { environment } from '../../../environments/environment.prod';

@Injectable()
export class BuscaPontosVendaService {

  constructor(private httpClient: HttpClient) { }

  buscarPontoVendaPorNome(nome: String): Observable<PontoVendaModel[]> {
    return this.httpClient.get<PontoVendaModel[]>('http://localhost:49478/api/PontoVenda/ConsultarPontoVendaPorNome?nome=' + nome);
  }

}
