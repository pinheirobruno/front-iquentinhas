export interface UsuarioModel {

    nomeCompleto: string;
    email: string;
    telefone: string;
    senha: string;
}