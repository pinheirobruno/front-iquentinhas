export interface PontoVendaModel {

    nome: String;
    CEP: String;
    rua: String;
    numero: String;
    cidade: String;
    bairro: String;
    complemento: String;

}