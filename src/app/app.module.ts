import { LoginComponent } from './account/login/login.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { PaginaInicialModule } from './pagina-inicial/pagina-inicial.module';
import { AccountModule } from './account/account.module';
import { CadastroComponent } from './pagina-inicial/cadastro/cadastro.component';
import { BuscaPontosVendaService } from './shared/services/busca-pontos-venda.service';
import { AccountService } from './shared/services/account.service';
import { HomePageComponent } from './home-page/home-page.component';

const appRoutes: Routes = [
  { path: '',
  component: HomePageComponent },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'cadastrar',
    component: CadastroComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes
    ),
    BrowserModule,
    PaginaInicialModule,
    AccountModule,
    HttpClientModule
  ],
  providers: [
    BuscaPontosVendaService,
    HttpClientModule,
    AccountService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
