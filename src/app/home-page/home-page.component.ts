import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { ElementRef } from '@angular/core';

import * as jQuery from 'jquery';

declare var $: any;

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  @ViewChild('myModal') myModal: ElementRef;

  constructor() { }

  ngOnInit() {
  }

  myFunc() {
    $('#myModal').modal('show');
  }

}
